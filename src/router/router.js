import Vue from 'vue';
import Router from 'vue-router';
import HeaderComponent from "@/components/HeaderComponent";

Vue.use(Router);

let router = new Router({
    routes: [
        {
            path: '/',
            name: 'main',
            component: HeaderComponent
        }
    ]
})

export default router;
